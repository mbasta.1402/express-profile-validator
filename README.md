# express-profile-validator

## Table of contents

- [What is express-profile-validator](#what-is-express-profile-validator)
- [Installation](#installation)
- [Previous considerations](#previous-considerations)
- [Methods](#methods)
  - [executeWithSession](#executewithsession)
  - [executeWithJWT](#executewithJWT)
- [Contributions](#contributions)


### What is express-profile-validator[⬆](#table-of-contents)
***express-profile-validator*** is a `middleware` for validating permissions on a resource of an **[express](https://expressjs.com/)** application.

Designed to be easy and quick to apply, as well as providing scalability during the development stage.

### Installation
Install module using

  `npm install express-profile-validator`

### Previous considerations[⬆](#table-of-contents)
To use this middleware is neccesary consider two things:
  1. If your express app use JWT like authentication method is necessary what JWT be sended like request `Authorization` header with `Bearer` type.

  2. Regardless of which authentication method your express application is using, you must ensure that in your JWT or Session there is an object that respects the `Profile` interface. 
  
  **Important:** Name this property/claim like `loggedProfile`.

  ```typescript
  interface Profile {
    id: number
    name: string
  }
  ```

### Methods[⬆](#table-of-contents)

### executeWithSession[⬆](#table-of-contents)
This method can be called when your express app is using `Session`, verify if `Request` session logged profile is included in `authorized profiles` set.

```typescript
import express from "express";

const router = express.Router();

router.get('/my-awesome-endpoint', ProfileAuthorization.executeWithSession(authorizedProfiles), myAwesomeController.myAwesomeFunction);
```

### executeWithJWT[⬆](#table-of-contents)
This method can be called when your express app is using `JWT`, verify if `Token claim` logged profile is included in `authorized profiles` set.

```typescript
import express from "express";

const router = express.Router();

router.get('/my-awesome-endpoint', ProfileAuthorization.executeWithJWT(authorizedProfiles), myAwesomeController.myAwesomeFunction);
```

### Usage
This middleware is designed to be used at the **route level** 
as we saw in [methods](#methods):

```typescript
router.get('/my-awesome-endpoint', ProfileAuthorization.executeWithSession(authorizedProfiles), myAwesomeController.myAwesomeFunction);
```

or at the **router level**:

```typescript
router.use(ProfileAuthorization.executeWithSession(authorizedProfiles));

router.get('/my-awesome-endpoint', myAwesomeController.myAwesomeFunction);

router.post('/my-awesome-post-endpoint', myAwesomeController.myAwesomeCreateFunction);
```

*In this last example both endpoints will be affected by the middleware, and all those added in the future.*

### Contributions
Contributions are welcome! Please feel free to submit a pull request.