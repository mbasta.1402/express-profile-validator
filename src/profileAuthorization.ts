import { NextFunction, Request, Response } from 'express'
import { jwtDecode } from 'jwt-decode'
import { Profile } from './profile'

export class ProfileAuthorization {
  protected readonly a: string = 'A';
  /**
   * Return a middleware that checks if the user logged has the required profile
   * 
   * @param authorizedProfiles 
   * @returns 
   */
  public static executeWithSession = (authorizedProfiles: Set<string>) => {
    return (req: Request, res: Response, next: NextFunction) => {
      if (!authorizedProfiles.has(this.getSessionProfileName(req))) {
        res.status(403).json({ message: 'Unauthorized' })
        return;
      }

      next();
    };
  };

  /**
   * Return the name of the profile of the user logged in the session
   */
  private static readonly getSessionProfileName = (req: any): string => {
    return req.session.user.loggedProfile.name
  };

  /**
   * Return a middleware that checks if the user logged has the required profile
   * 
   * @param authorizedProfiles 
   * @returns 
   */
  public static executeWithJWT = (
    authorizedProfiles: Set<string>,
  ) => {
    return (req: Request, res: Response, next: NextFunction) => {
      if (!authorizedProfiles.has(this.getJWTProfileName(req))) {
        res.status(403).json({ message: 'Unauthorized' })
      }
  
      next()
    }
  };

  /**
   * Return the name of the profile of the user logged in jwt token
   */
  private static readonly getJWTProfileName = (req: Request): string => {
    const profile = jwtDecode<any>(
      req.headers.authorization!.split(' ')[1],
    ) as Profile;

    return profile.name;
  };
}
